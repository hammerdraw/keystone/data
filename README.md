Repository for tools and data for community-created cards for the Keystone Card Game,
an arcade map for the [StarCraft II](https://starcraft2.com).

### Copyright Notes
For non-commercial use only.
StarCraft II is a registered trademark of Blizzard Entertainment.
All images presented in this repository are taken from public sources.
All card data is created by holders of [Keystone Creative Community Discord Server](https://discord.gg/gd75HXS), also known as 

### Links
 - [Keystone' author's blog](https://submapdesign.wordpress.com/)
 - [Keystone Official Community Discord Server](https://discordapp.com/invite/Q6jMh6c)
 - [Keystone Creative Community Discord Server](https://discord.gg/gd75HXS)
 - [Keystone Subreddit](https://www.reddit.com/r/keystonesc/)
